import React, { useEffect } from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom'
import Login from './pages/login'
import Ruc from './pages/ruc'
import Layout from "./components/layouts";

function MainRouter() {
    return (
        <Router>
            <Layout>
                <Switch>
                    <Route exact path='/' component={Login}  />
                    <Route exact path='/ruc' component={Ruc} />
                </Switch>
            </Layout>
        </Router>
    )
}

export default MainRouter
