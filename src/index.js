import React, { useEffect } from 'react';
import ReactDOM from 'react-dom'
import MainRouter from './router'
import './style.css'
import './styles/main.scss'
import {QueryClient, QueryClientProvider} from "react-query";

const initialState = {}
const queryClient = new QueryClient()
ReactDOM.render(
    <React.StrictMode>
        <QueryClientProvider client={queryClient}>
            <MainRouter />
        </QueryClientProvider>
    </React.StrictMode>,
    document.getElementById('root')
);
