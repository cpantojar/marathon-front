import { Button, CircularProgress, Container, CssBaseline, makeStyles, TextField, Typography } from '@material-ui/core'
import React, { useEffect } from 'react'
import { useForm, Controller } from 'react-hook-form'
import {useQuery} from "react-query";
import axios,{AxiosError} from "axios"
const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}))


function Ruc() {
    const classes = useStyles()
    const { handleSubmit, errors, control } = useForm()

    const { data } =  axios.get(
        "http://wsruc.com/Ruc2WS_JSON.php?tipo=2&ruc=20600892470&token=cXdlcnR5bGFtYXJja19zYUBob3RtYWlsLmNvbXF3ZXJ0eQ=="
    );
    if(data){
        console.log(data)
    }
    const onSubmit = (data) => {
        console.log("correcto!")
        console.log(data);

    }
    return (
        <Container component='main' maxWidth='xs'>
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component='h1' variant='h5'>
                    Consultar RUC
                </Typography>
                <form onSubmit={handleSubmit(onSubmit)} className={classes.form} noValidate>
                    <Controller
                        name='ruc'
                        id='ruc'
                        control={control}
                        defaultValue=''
                        render={({ onChange, value }) => (
                            <TextField
                                variant='outlined'
                                margin='normal'
                                required
                                fullWidth
                                onChange={onChange}
                                value={value}
                                label='Ingresar Numero de ruc'
                                autoComplete='ruc'
                                autoFocus
                            />
                        )}
                    />
                    <Button
                        type='submit'
                        fullWidth
                        variant='contained'
                        color='primary'
                        className={classes.submit}
                    >
                        Consultar
                    </Button>
                </form>
            </div>
        </Container>
    )
}

export default Ruc
