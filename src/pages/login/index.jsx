import { Button, CircularProgress, Container, CssBaseline, makeStyles, TextField, Typography } from '@material-ui/core'
import React, { useEffect } from 'react'
import { useForm, Controller } from 'react-hook-form'
import {useHistory} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}))


function Login() {
    const classes = useStyles()
    const { handleSubmit, errors, control } = useForm()
    const history = useHistory()
    const onSubmit = (data) => {
        history.push('/ruc')
    }
     return (
        <Container component='main' maxWidth='xs'>
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component='h1' variant='h5'>
                    Ingresar a Marathon
                </Typography>
                <form onSubmit={handleSubmit(onSubmit)} className={classes.form} noValidate>
                    <Controller
                        name='username'
                        id='username'
                        control={control}
                        defaultValue=''
                        render={({ onChange, value }) => (
                            <TextField
                                variant='outlined'
                                margin='normal'
                                required
                                fullWidth
                                onChange={onChange}
                                value={value}
                                label='Nombre de usuario'
                                autoComplete='username'
                                autoFocus
                            />
                        )}
                    />
                    <Controller
                        name='password'
                        id='password'
                        control={control}
                        defaultValue=''
                        render={({ onChange, value }) => (
                            <TextField
                                variant='outlined'
                                margin='normal'
                                required
                                type='password'
                                fullWidth
                                onChange={onChange}
                                value={value}
                                label='Password'
                                autoComplete='current-password'
                            />
                        )}
                    />
                    <Button
                        type='submit'
                        fullWidth
                        variant='contained'
                        color='primary'
                        className={classes.submit}
                    >
                        Ingresar
                    </Button>
                </form>
            </div>
        </Container>
    )
}

export default Login
